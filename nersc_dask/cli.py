
import sys
assert sys.version_info >= (3, 0), "This script only works with Python > 2"

from .nersc_dask import main

def cli():
    main()
